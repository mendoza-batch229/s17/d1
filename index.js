console.log("Hello World")

// [SECTION] - Function
// Functions in JS are lines?blocks of codes that will tell our device/application to perform a particular task.
// Function are  mostly created to create complicated task to run several lines of code succession.
// They are also used to prevent repeating lines of codes.

// Function declaration - "function"

/*
Syntax:
function functionname(){
	code block
	let number = 10;
}
*/

function printname(){
	console.log("My name is AL ZAILO");
}
// calling for invoke/invocation
printname();

// [SECTION] Fucntion Invocation
// sampleInvocation(); undeclared function cannot be invoke

// [SECTION] function declaration vs expresssion

function declaredFunction(){
	console.log("Hello World from declaredFunction()");
}
declaredFunction();

// This is a declared function
// A function ncan be stored in a variable. This is called fucntion expression.
// Anonymous function - a function without a name.

// variableFunction();
/*

	error - function expression being restored in a let or const, cannot be hoisted.

*/

// ananymous function
let variablefunction = function(){
	console.log("Hello Again!");
}
variablefunction();

let functionExpression = function funcName(){
	console.log("Hello From The Other Side!");
}
functionExpression();   //This is the right invocation in function expression
// funcName(); This will return error


// You can re-assign declared functions and function expression to a new ananymous function.
declaredFunction = function(){
	console.log("Updated declaredFunction()")
}
declaredFunction();

functionExpression = function(){
	console.log("updated functionExpression()");
}
functionExpression();

// However, we cannot re-assign a function expression initialized  with const.

const constantFunction = function(){
	console.log("Initialized with const!");
}
constantFunction();

/*constantFunction = function(){
	console.log("Will try to re-assign()");
}
constantFunction();   cannot re-assign if it is const
*/



// [SECTION] - Function Scoping
/*
1 Local/block scoping
2 Global Scope
3 Function Scope
*/

{
	// This local variable is only accessible inside our curly braces
	let localVariable = "Armando Perez";
	console.log(localVariable);
}

// This is a global variable and it its value is accessible anywhere in the code base
let globalVariable = "Mr. WorldWide";

console.log(globalVariable);


function showName(){
	// Function scoped variables
	var  functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}
showName();
	


// Nested Function

function myNewFunction(){
	let name = "Jason";

	function nestedFunction(){
		let nestedName= "John";
		console.log(name);
	}
	nestedFunction();
	// console.log(nestedName); error function scoped
}
myNewFunction();

// Function and Globalscope variables

let globalName = "Alexandro";
function myNewFunction2(){
	let nameInside = "Renz";

	console.log(globalName);
}
myNewFunction2();

// [SECTION] - Using Alert
// Alert() allows us to show small window  at the top of our browser.

// alert("Hello World!");


// function showSampleAlert(){
// 	alert("Hello User!")
// }

// showSampleAlert();


// console.log("I will only log in the console when alert is dismiss");

// Notes about or use of our alert
	// Show only alert() for short dialog message.
	// Do not over use alert() because js has to wait to be dismiss before continuing.


// [SECTION] - Using prompt()
// prompt() allows us to show a small window and gather user input.
// Usually prompt are stored in a variable.



// let samplePrompt = prompt("Enter your Name.");

// console.log("Hello, " + samplePrompt);
// console.log(typeof samplePrompt);

// let sampleNullPrompt = prompt("Do not enter anything");
// console.log(sampleNullPrompt);  //return an empty string

// function printWelcomeMessage(){
// 	let firstName = prompt("Enter your first name");
// 	let lastName = prompt("Enter your last name");

// 	console.log("Hello, " + firstName + " " + lastName + "!");
// 	console.log("Welcome to my page!");
// }

// printWelcomeMessage();


// // [SECTION] - Function naming convention

// function getCourse(){
// 	let courses = ["Science 101", "Math 101", "English 101"];
// 	console.log(courses);
// }
// getCourse();


